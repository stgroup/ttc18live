#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import os
import subprocess
import multiprocessing
import sys
import time
import yaml


def work(cmd):
    print '[{}] Calling {}'.format(multiprocessing.current_process(), ' '.join(cmd))
    subprocess.call(cmd)
    sys.stdout.flush()
    time.sleep(1)
    return cmd


if __name__ == '__main__':
    with open('push-multiple-to-influx.yml') as fdr:
        content = yaml.load(fdr)
    # print content
    base_dir = content['base_dir']
    python_executable = content['python_executable']
    solutions = content['solutions']
    sizes = content['sizes']
    queries = content['queries']

    count = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(processes=count)
    commands = []

    for solution_name in solutions:
        fileformat = solutions[solution_name]['fileformat']
        for size in sizes:
            size = str(size)
            for query in queries:
                query = str(query)
                basename = fileformat.replace(r'%size', size).replace(r'%query', query)
                filename = os.path.join(base_dir, solution_name, basename)
                if not os.path.exists(filename):
                    print 'File "{}" not found'.format(filename)
                    continue
                cmd = [python_executable, '-f', filename,
                       '-s', size,
                       '-q', query,
                       '-n', solution_name]
                commands.append(cmd)
    print len(commands)
    results = []
    r = pool.map_async(work, commands, callback=results.append)
    r.wait()
    print len(results)
