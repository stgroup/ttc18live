# TTC 2018 LiveContest: JastAdd solution(s)

This repository contains the source for the JastAdd solutions for the [TTC 2018 LiveContest](https://www.transformation-tool-contest.eu/2018/solutions_liveContest.html) described in detail in [the solution paper](http://ceur-ws.org/Vol-2310/paper8.pdf).
The source code for running this (and every other solution) can be found at the [github repository](https://github.com/TransformationToolContest/ttc2018liveContest).

## Running the example

In order to run the example, Gradle is used. Using the Gradle wrapper is important.
To get the tests running, one symlink is needed:

```bash
> ln -s $TTC_REPO/models solve/src/test/resources/
```
