target="../ttc2018liveContest/solutions"
# skipping "namelookup-xml-*"
for s in relast-* ; do
  echo ">> Building $s"
  ./gradlew ":${s}:installDist"
  solutionDir="$target/jastadd-$s"
  echo ">> Copy to $solutionDir"
  rm -rI "$solutionDir"
  mkdir -p "$solutionDir/bin"
  cp solution.ini "$solutionDir/"
  cp "solve/README-for-TTC-repo.md" "$solutionDir/README.md"
  cp "${s}/build/libs/solve-1.0-SNAPSHOT.jar" "${solutionDir}/bin/solve.jar"
done

# source code
echo "Copying source"
copySourceDir="$target/jastadd-source"
rm -rI "$copySourceDir"
mkdir -p "$copySourceDir/src"
cp -a solve/gradle-parts solve/build.gradle solve/jastadd_modules solve/.gitignore "$copySourceDir/"
cp -a solve/src/main "$copySourceDir/src/"
cp "solve/README-for-TTC-repo.md" "$copySourceDir/README.md"
