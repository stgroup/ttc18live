#!/usr/bin/env sh
mkdir -p "results"
now=$(date -u "+%Y-%m-%d_%H-%M-%S")
query=2
size=32
# namelookup-xml-*
for size in 8 16 32 64 128; do
  for d in relast-*; do
    ./timedBenchmark.sh "$d" $query $size > "results/results-${now}-${d}-${size}-Q${query}.csv"
  done
done
echo "Tool;View;ChangeSet;RunIndex;Iteration;PhaseName;MetricName;MetricValue" > "results/merged-${now}.csv"
cat results/results-${now}-* >> "results/merged-${now}.csv"
