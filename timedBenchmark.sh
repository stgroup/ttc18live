# Example call: "./timedBenchmark.sh relast-xml-inc 2 8"
project=$1
query=$2
size=$3
Tool=$project RunIndex=0 ChangeSet=$size ChangePath=solve/src/test/resources/models/$size/ Sequences=20 Query=Q$query java -jar "$project/build/libs/solve-1.0-SNAPSHOT.jar"
