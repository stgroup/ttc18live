#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import argparse
import csv
from datetime import datetime
import re
import os
import subprocess

from influxdb import InfluxDBClient
from influxdb import SeriesHelper

# InfluxDB connections settings
host = '172.22.1.152'
port = 8086
user = ''
password = ''
dbname = 'jastadd'

myclient = InfluxDBClient(host, port, user, password, dbname)

# myclient.create_retention_policy('awesome_policy', '3d', 3, default=True)


class MySeriesHelper(SeriesHelper):
    """Instantiate SeriesHelper to write points to the backend."""

    class Meta:
        """Meta class stores time series helper configuration."""

        # The client should be an instance of InfluxDBClient.
        client = myclient

        # The series name must be a string. Add dependent fields/tags
        # in curly brackets.
        series_name = 'ttc18live'

        # Defines all the fields in this time series.
        fields = ['dummy']

        # Defines all the tags for the series.
        tags = ['event', 'attribute', 'size', 'query', 'solution']

        # Defines the number of data points to store prior to writing
        # on the wire.
        bulk_size = 100000

        # autocommit must be set to True when using bulk_size
        autocommit = True


def nice_tag(s):
    s = s.replace('.', '_')
    s = re.sub(r'\([^)]*\)', '', s)
    return s


def wccount(filename):
    out = subprocess.Popen(['wc', '-l', filename],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT
                           ).communicate()[0]
    return int(out.partition(b' ')[0])


def main(args):
    if args.drop_database:
        myclient.drop_database(dbname)
        myclient.create_database(dbname)
    fieldnames = ['timestamp'] + MySeriesHelper.Meta.tags[:] + MySeriesHelper.Meta.fields[:]
    # read <filename> (or solve/<filename> if not found. maybe we are in root directory after all)
    filename = args.filename
    if not os.path.exists(filename):
        filename = os.path.join('solve', filename)
    with open(filename) as fdr:
        reader = csv.DictReader(fdr, fieldnames=fieldnames)

        for row in reader:
            dt = datetime.fromtimestamp(long(row['timestamp']) / 1000.0)
            MySeriesHelper(time=dt.strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
                           event=nice_tag(row['event']),
                           attribute=nice_tag(row["attribute"]),
                           size=args.size,
                           query=args.query,
                           solution=args.name,
                           dummy='1')
            # print MySeriesHelper._json_body_()
            # 1/0

    # print MySeriesHelper._json_body_()
    # To manually submit data points which are not yet written, call commit:
    MySeriesHelper.commit()

    # To inspect the JSON which will be written, call _json_body_():
    # print MySeriesHelper._json_body_()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", help="Filename to process", required=True)
    parser.add_argument("-s", "--size", help="Size of change set", required=True)
    parser.add_argument("-q", "--query", help="Computed query", required=True)
    parser.add_argument("-n", "--name", help="Name of the solution", default='jastadd-ttc18')
    parser.add_argument("--drop_database", action='store_true',
                        help="Whether the database should be dropped beforehand (Default: false)")
    args = parser.parse_args()

    main(args)
