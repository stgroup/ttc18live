package de.tudresden.inf.st.ttc18live.test;

import de.tudresden.inf.st.ttc18live.AbstractLiveContestDriver;
import de.tudresden.inf.st.ttc18live.LiveContestDriverXml;

/**
 * Test against given expected query results using XML solution.
 *
 * @author rschoene - Initial contribution
 */
public class AllXmlTest extends AbstractAllTest {

  public AllXmlTest(int query, int size, String name) {
    super(query, size, name);
  }

  @Override
  AbstractLiveContestDriver createDriver() {
    return new LiveContestDriverXml();
  }
}
