package de.tudresden.inf.st.ttc18live.test;

import com.github.stefanbirkner.systemlambda.Statement;
import com.github.stefanbirkner.systemlambda.SystemLambda;
import com.opencsv.CSVReader;
import de.tudresden.inf.st.ttc18live.AbstractLiveContestDriver;
import de.tudresden.inf.st.ttc18live.LiveContestDriverXml;
import org.junit.*;
//import org.junit.contrib.java.lang.system.EnvironmentVariables;
//import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Test template against given expected query results.
 *
 * @author rschoene - Initial contribution
 */
@RunWith(Parameterized.class)
public abstract class AbstractAllTest {
//  @Rule
//  public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
//  @Rule public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

  private final static int maxSequence = 5;
  private final static Path modelsPath = Paths.get("src", "test", "resources", "models");

  private final int query;
  private final int size;
  private final Map<Integer, String> localExpected;

  @Parameterized.Parameters(name = "{2}")
  public static Collection<Object[]> data() {
    // query, size
    Collection<Object[]> result = new ArrayList<>();
    for (int query = 1; query <= 2; query++) {
      for (int size : new int[]{1, 2, 4, 8}) {
        result.add(new Object[]{query, size, "Q" + query + "@" + size});
      }
    }
    return result;
  }

  /** query -> size -> { iteration -> value } */
  private static Map<Integer, Map<Integer, Map<Integer, String>>> expected = new HashMap<>();

  @BeforeClass
  public static void readExpectedResults() {
    Assert.assertTrue("Please create a link to the models directory of 'https://github.com/TransformationToolContest/ttc2018liveContest/' in 'src/test/resources/'",
        modelsPath.toFile().exists());
    InputStream inputStream = AllXmlTest.class.getResourceAsStream("/expected-results.csv");
    try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
         CSVReader reader = new CSVReader(inputStreamReader, ';', '"', 1)) {
      reader.iterator().forEachRemaining( line -> {
        String queryString = line[0];
        String sizeString = line[1];
        String iterationString = line[2];
//        String phase = line[3];
        String value = line[4];
        Integer query = Integer.valueOf(queryString.substring(1));
        Integer size = Integer.valueOf(sizeString);
        Integer iteration = Integer.valueOf(iterationString);
        expected.putIfAbsent(query, new HashMap<>());
        expected.get(query).putIfAbsent(size, new HashMap<>());
        expected.get(query).get(size).putIfAbsent(iteration, value);
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void withVars(Statement statement) throws Exception {
    SystemLambda.withEnvironmentVariable("ChangeSet", Integer.toString(size))
        .and("ChangePath", modelsPath.resolve(Integer.toString(size)).toFile().getAbsolutePath())
        .and("Sequences", Integer.toString(maxSequence))
        .and("Query", "Q" + query)
        .execute(statement);
  }

  private AbstractLiveContestDriver driver;

  public AbstractAllTest(int query, int size, @SuppressWarnings("unused") String name) {
    this.query = query;
    this.size = size;
    this.localExpected = expected.get(query).get(size);
  }

  abstract AbstractLiveContestDriver createDriver();

  @Test
  public void test() throws Exception {
    System.out.println(query + "," + size + ":" + expected.get(query).get(size));
    String systemOut = SystemLambda.tapSystemOut(() -> withVars(() -> {
      driver = createDriver();
      driver.mainImpl();
    }));
    Map<Integer, String> actual = new HashMap<>();
    for (String line : systemOut.split("\n")) {
      String[] tokens = line.split(";");
      if ("Elements".equals(tokens[6])) {
        int iteration = Integer.parseInt(tokens[4]);
        actual.put(iteration, tokens[7]);
      }
    }
    for (int iteration = 0; iteration <= maxSequence; iteration++) {
      Assert.assertTrue("No result found for iteration " + iteration, actual.containsKey(iteration));
      Assert.assertEquals("Wrong elements", localExpected.get(iteration), actual.get(iteration));
    }
  }
}
