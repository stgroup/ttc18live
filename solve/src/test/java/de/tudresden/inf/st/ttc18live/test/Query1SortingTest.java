package de.tudresden.inf.st.ttc18live.test;

import de.tudresden.inf.st.ttc18live.jastadd.model.*;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * TODO: Add description.
 *
 * @author rschoene - Initial contribution
 */
public class Query1SortingTest {

  private static final int SEED = 42;

  private SocialNetwork create(int... scores) {
    SocialNetwork result = SocialNetwork.createSocialNetwork();
    Random random = new Random(SEED);
    long timeOfToday = System.currentTimeMillis();

    // one user to submit all posts and comments.
    User submitter = new User();
    submitter.setId(7000L);
    result.addUser(submitter);

    long postIdCounter = 0L;
    long commentIdCounter = 1000L;
    for (int scoreIndex = 0; scoreIndex < scores.length; scoreIndex++) {
      int score = scores[scoreIndex];
      // always need at least one comment
      if (score < 10) {
        scores[scoreIndex] = score = score + 10;
      }
      int commentsNeeded = score / 10;
      int likesNeeded = score % 10;
      Post post = new Post();
      post.setId(++postIdCounter);
      post.setTimestamp(timeOfToday);
      int indexLikedComment = random.nextInt(commentsNeeded);
      for (int i = 0; i < commentsNeeded; i++) {
        Comment comment = new Comment();
        comment.setId(++commentIdCounter);
        post.addComment(comment);
        if (i == indexLikedComment) {
          for (int j = 0; j < likesNeeded; j++) {
            User user = new User();
            user.setId(7001L + j);
            result.addUser(user);
            submitter.addSubmission(comment);
            user.addLike(comment);
          }
        }
      }
      result.addPost(post);
    }
    result.flushTreeCache();
    for (int i = 0; i < scores.length; i++) {
      assertEquals(scores[i], result.getPost(i).score());
    }
    return result;
  }

  private void printNetwork(SocialNetwork socialNetwork) {
    StringBuilder sb = new StringBuilder("Users = ");
    for (User user : socialNetwork.getUserList()) {
      sb.append(user.getId());
      if (user.getLikes().size() > 0) {
        sb.append("-likes->{");
        for (Comment comment : user.getLikes()) {
          sb.append(comment.getId()).append(",");
        }
        sb.append("}");
      }
      sb.append(", ");
    }
    sb.append("\nPosts = ");
    for (Post post : socialNetwork.getPostList()) {
      sb.append(post.getId()).append("{s:").append(post.score()).append(",c:");
      printComments(sb, post.getCommentList());
      sb.append("}, ");
    }
    System.out.println(sb.toString());
  }

  private void printComments(StringBuilder sb, List<Comment> commentList) {
    for (Comment comment : commentList) {
      sb.append(comment.getId());
      if (comment.getNumComment() > 0) {
        sb.append(":{");
        printComments(sb, comment.getCommentList());
        sb.append("}");
      }
      sb.append(",");
    }
  }

  private void assertQuery1(SocialNetwork sut, long firstId, long secondId, long thirdId) {
    printNetwork(sut);
    String actualAnswer = sut.query(1);
    String[] ids = actualAnswer.split("\\|");
    System.out.println("Got '" + actualAnswer + "'");
    assertEquals(3, ids.length);
    assertEquals(firstId, Long.valueOf(ids[0]).longValue());
    assertEquals(secondId, Long.valueOf(ids[1]).longValue());
    assertEquals(thirdId, Long.valueOf(ids[2]).longValue());
  }

  @Test
  public void testSort1() {
    SocialNetwork sut = create(1, 2, 3);
    assertQuery1(sut, 3, 2, 1);
  }

  @Test
  public void testSort2() {
    SocialNetwork sut = create(1, 2, 3, 4);
    assertQuery1(sut, 4, 3, 2);
  }

  @Test
  public void testSort3() {
    SocialNetwork sut = create(3, 2, 1);
    assertQuery1(sut, 1, 2, 3);
  }

  @Test
  public void testSort4() {
    SocialNetwork sut = create(1, 3, 1, 1, 1, 2, 4, 1);
    assertQuery1(sut, 7, 2, 6);
  }
}
