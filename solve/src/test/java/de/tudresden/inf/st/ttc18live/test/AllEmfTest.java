package de.tudresden.inf.st.ttc18live.test;

import de.tudresden.inf.st.ttc18live.AbstractLiveContestDriver;
import de.tudresden.inf.st.ttc18live.LiveContestDriverEMF;

/**
 * Test against given expected query results using EMF solution.
 *
 * @author rschoene - Initial contribution
 */
public class AllEmfTest extends AbstractAllTest {

  public AllEmfTest(int query, int size, String name) {
    super(query, size, name);
  }

  @Override
  AbstractLiveContestDriver createDriver() {
    return new LiveContestDriverEMF();
  }
}
